import 'package:flutter/material.dart';
import 'movie.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

Future<Movies> fetchFavorite() async {
  final response =
  await http.get('https://movies-087d.restdb.io/rest/favoritemovies',headers:  {
    "content-type": "application/json",
    "x-apikey": "9d676be63052503a05bb34e96068725d35278",
    "cache-control": "no-cache"
  });

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON

    return Movies.favorites(json.decode(response.body));
  }
  else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<Movies> fetchSearch(String movie,int x) async {
  final response =
  await http.get('https://api.themoviedb.org/3/search/movie?api_key=2df757a6eae094b792c82b543ffd7d81&query=$movie&page=$x');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Movies.fromJson(json.decode(response.body));
  }
  else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class Movies {
  final int totalResult;
  final bool response;
  final List<Movie> search;

  Movies({this.totalResult,this.response,this.search}){
  }


  Movies.error(this.response);

  factory Movies.fromJson(Map<String, dynamic> json) {
    bool response=false;
    List<Movie> search = new List<Movie>();

    if (json['total_results']!=0){
      response=true;
      for (Map<String, dynamic> movie in (json['results'])){
        String date = (movie["release_date"]);
        if (date.length>=4){
          date = date.substring(0,4);
        }
        search.add(new Movie(
            movie['original_title'],date,movie["id"].toString(),movie["poster_path"]
        ));
      }
      return Movies(
        totalResult: json['total_results'],
        response: response,
        search: search,
      );
    }
    else{
      return Movies.error(false);
    }
  }

  factory Movies.favorites(List<dynamic> json) {
    List<Movie> search = new List<Movie>();
    for (Map<String, dynamic> movie in (json)){
      search.add(new Movie.favorite(
          movie['title'],movie['year'],movie["id"],movie["poster"],movie["_id"]
      ));
    }
    return Movies(
      totalResult: search.length,
      response: true,
      search: search,
    );
  }
}

class Movie {
  final String title;
  final String year;
  final String id;
  final String poster;
  final String id2;

  Movie(this.title, this.year, this.id, this.poster);

  Movie.favorite(this.title, this.year, this.id, this.poster,this.id2);

}

class SearchScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return
      MaterialApp(
        theme: new ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
          // counter didn't reset back to zero; the application is not restarted.
          primarySwatch: Colors.red,
        ),
        title: "Movies OMDB",
        home: new SearchScreenIsi()
      );
  }
}

class SearchScreenIsi extends StatefulWidget {
  SearchScreenState createState() => new SearchScreenState();
}

class SearchScreenState extends State<SearchScreenIsi> with SingleTickerProviderStateMixin{
  int halaman;
  int totalHalaman;
  TextEditingController controller ;
  TabController _tabController;
  var movies;
  var mainContent;
  List<Widget> action = new List<Widget>();

  void search(int x){
    halaman = x;
    totalHalaman =1;
    setState(() {
      mainContent = FutureBuilder<Movies>(
        future: fetchSearch(controller.text,x),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {

            case ConnectionState.none: return new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text("Movie not found!")
              ],
            );
            case ConnectionState.waiting:
              return new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new CircularProgressIndicator()
                ],
              );

            default:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
              if (snapshot.hasData) {
                if (snapshot.data.response){
                  var totalResult = snapshot.data.totalResult;
                  if (totalResult>20){
                    totalHalaman = (totalResult/20).ceil();
                    totalResult=20;
                  }
                  var isi =
                  new ListView.builder(
                    itemCount: totalResult,
                    itemBuilder: (context, index) {
                      var img = snapshot.data.search[index].poster;
                      if (img == null){
                        img="https://www.transparenttextures.com/patterns/asfalt-light.png";
                      }
                      else{
                        img = "https://image.tmdb.org/t/p/w500"+snapshot.data.search[index].poster;
                      }
                      var image =
                      Image.network(
                        img,
                        fit: BoxFit.fitHeight,
                      );
                      return new ListTile(
                        leading: new Container(
                            width: 40.0,
                            height: 70.0,
                            child: image
                        ),
                        title: new Text(snapshot.data.search[index].title),
                        subtitle: new Text(snapshot.data.search[index].year),
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => new MovieScreen(
                                id:snapshot.data.search[index].id
                            )
                            ),
                          );
                        },
                      );
                    },
                  );
                  List<DropdownMenuItem<int>> list = new List<DropdownMenuItem<int>>();
                  for (var i = 1 ; i<=totalHalaman ; i++){
                    list.add(
                      new DropdownMenuItem<int>(child: new Center(child:new Text("  Page $i")),value: i),
                    );
                  }
                  return new Column(
                    children: <Widget>[
                      new Expanded(child: isi, flex:1),
                      new Container(
                        color:Colors.white,
                        child:
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children:<Widget>[
                            new Expanded(child:
                              new DropdownButton(
                                value:x,
                                items: list,
                                onChanged: (newVal){
                                  halaman = newVal;
                                  search(newVal);
                                }
                              )
                            )
                          ],
                        ),
                      )
                    ],
                  );
                }
                else{
                  return new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text("Movie not found!")
                    ],
                  );
                }
              }
            }
          },
      );
    });
  }

  @override
  void initState() {
    super.initState();
    mainContent =new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text("Search Movies")
      ],
    );
    controller = new TextEditingController();
    _tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
        ],
        bottom: TabBar(
          controller: _tabController,
          tabs: [
            Tab(icon: Icon(Icons.search)),
            Tab(icon: Icon(Icons.favorite)),
          ],
        ),
        title: Text('Cinema Searchs'),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          new Center(
            child: new Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Expanded(
                  flex: 1,
                  child: mainContent,
                ),
                new Container(
                  height: 50.0,
                  color:Colors.red,
                  child:
                  new Row(
                    children: <Widget>[
                      new Container(
                        width: 5.0,
                      ),
                      new Expanded(
                          child:
                          new TextField(
                            onSubmitted:(aa) {
                              search(1);
                            } ,
                            decoration: InputDecoration(
                                hintText: 'Search here..',
                                fillColor: Colors.white,
                                filled: true
                            ),
                            controller: controller,
                          )
                      ),
                      new Container(
                        width: 50.0,
                        child:new FlatButton(
                          child:
                          new Icon(Icons.search,color:Colors.white),
                          onPressed: () {
                            FocusScope.of(context).requestFocus(new FocusNode());
                            search(1);
                          },
                        ),
                      ),

                    ],
                  ),
                )
              ],
            )
          ),
          new Center(
            child: new Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Expanded(
                  flex: 1,
                  child: FutureBuilder<Movies>(
                    future: fetchFavorite(),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {

                        case ConnectionState.none: return new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text("Movie not found!")
                          ],
                        );
                        case ConnectionState.waiting:
                          return new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new CircularProgressIndicator()
                            ],
                          );

                        default:
                          if (snapshot.hasError)
                            return new Text('Error: ${snapshot.error}');
                          else
                          if (snapshot.hasData) {
                            if (snapshot.data.response){
                              var totalResult = snapshot.data.totalResult;
                              if (totalResult>10){
                                totalHalaman = (totalResult/10).ceil();
                                totalResult=10;
                              }
                              return new ListView.builder(
                                itemCount: totalResult,
                                itemBuilder: (context, index) {
                                  var img = snapshot.data.search[index].poster;
                                  if (img == "N/A"){
                                    img="https://www.transparenttextures.com/patterns/asfalt-light.png";
                                  }
                                  var image =
                                  Image.network(
                                    img,
                                    fit: BoxFit.fitHeight,
                                  );
                                  return new ListTile(
                                    leading: new Container(
                                        width: 40.0,
                                        height: 70.0,
                                        child: image
                                    ),
                                    title: new Text(snapshot.data.search[index].title),
                                    subtitle: new Text(snapshot.data.search[index].year),
                                    onTap: (){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => new MovieScreen(
                                            id:snapshot.data.search[index].id,
                                            id2: snapshot.data.search[index].id2,
                                        )
                                        ),
                                      );
                                    },
                                  );
                                },
                              );
                            }
                            else{
                              return new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text("Movie not found!")
                                ],
                              );
                            }
                          }
                      }
                    },
                  )
                ),
                new Container(
                  color:Colors.red,
                  height: 50.0,
                  child:
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        flex:1,
                        child:new FlatButton(
                          child:
                          new Icon(Icons.add,color:Colors.white),
                          onPressed: () {
                            _tabController.animateTo(0);
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
          ),
        ],
      ),
    );
  }
}