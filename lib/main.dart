import 'package:flutter/material.dart';
import 'search.dart';

void main(){
  MaterialPageRoute.debugEnableFadingRoutes = true;
  runApp(new SearchScreen());
}