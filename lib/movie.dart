import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

class MovieDetail {
  final String title;
  final String year;
  final String genre;
  final String tagline;
  final String prod;
  final String plot;
  final String rating;
  final String language;
  final String poster;


  MovieDetail({this.title, this.year, this.genre, this.tagline, this.prod,
      this.plot, this.rating, this.language,this.poster});

  factory MovieDetail.fromJson(Map<String, dynamic> json) {
    String date = (json["release_date"].toString());
    if (date.length>=4){
      date = date.substring(0,4);
    }
    String genre = "";
    String prod = "";
    String lang = json["original_language"];
    if (lang==null){
      lang="";
    }
    if (json["production_companies"].length>0){
      prod = json["production_companies"][0]["name"];
    }
    for (Map<String,dynamic> a in json["genres"]){
      genre+=a["name"]+"; ";
    }
    String path="https://www.transparenttextures.com/patterns/asfalt-light.png";
    if (json["poster_path"]!=null){
      path = "https://image.tmdb.org/t/p/w500"+json["poster_path"];
    }
    return MovieDetail(
      title: json["title"],
      year: date,
      genre: genre,
      tagline: json["tagline"],
      prod: prod,
      plot: json["overview"],
      rating: json["vote_average"].toString(),
      language: lang,
      poster: path,
    );
  }
}

class MovieScreen extends StatelessWidget{
  final String id;
  final String id2;

  MovieScreen({this.id,this.id2});

  @override
  Widget build(BuildContext context) {
    return
      new MovieIsi(id:this.id,id2:this.id2);
  }
}

class MovieIsi extends StatefulWidget {
  final String id;
  final String id2;

  MovieIsi({this.id,this.id2});

  MovieState createState() => new MovieState(id:this.id,id2:this.id2);
}

class MovieState extends State<MovieIsi>{
  var movieJson;
  final String id;
  String id2;
  var floatingButton0;
  var floatingButton1;

  MovieState({this.id,this.id2});

  Future<MovieDetail> fetchPost() async {
    final response =
    await http.get("https://api.themoviedb.org/3/movie/$id?api_key=2df757a6eae094b792c82b543ffd7d81");

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      movieJson = json.decode(response.body);
      return MovieDetail.fromJson(json.decode(response.body));
    }
    else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<FloatingActionButton> checkFavorite() async {
    final response =
    await http.get('https://movies-087d.restdb.io/rest/favoritemovies?q={"id":"$id"}',headers:  {
      "content-type": "application/json",
      "x-apikey": "9d676be63052503a05bb34e96068725d35278",
      "cache-control": "no-cache"
    });

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON

      if(json.decode(response.body).length==0){
        return floatingButton0;
      }
      else{
        return floatingButton1;
      }
    }
    else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<FloatingActionButton> setFavorite(bool status) async {
    if (status){
      final response =
      await http.delete('https://movies-087d.restdb.io/rest/favoritemovies/$id2',headers:  {
        "content-type": "application/json",
        "x-apikey": "9d676be63052503a05bb34e96068725d35278",
        "cache-control": "no-cache"
      });
      print(response.statusCode);

      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON
        return floatingButton0;
      }
      else {
        // If that call was not successful, throw an error.
        throw Exception('Failed to load post');
      }
    }
    else{
      String date = (movieJson["release_date"]);
      if (date.length>=4){
        date = date.substring(0,4);
      }
      final response=
          await http.post('https://movies-087d.restdb.io/rest/favoritemovies',headers:  {
            "x-apikey": "9d676be63052503a05bb34e96068725d35278",
            "cache-control": "no-cache"
          }, body: {
            "id":"$id",
            "title":"${movieJson['title']}",
            "year":"${date}",
            "poster":"https://image.tmdb.org/t/p/w500${movieJson['poster_path']}",
          }
          );
      id2 = json.decode(response.body)["_id"];
      if (response.statusCode == 201) {
        // If the call to the server was successful, parse the JSON
        return floatingButton1;
      }
      else {
        // If that call was not successful, throw an error.
        throw Exception('Failed to load post');
      }
    }
  }

  var floatingButton = null;

  toggleFavorite(status){
    setState(() {
      floatingButton = FutureBuilder<FloatingActionButton>(
          future: setFavorite(status),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return null;
              case ConnectionState.waiting:
                return new CircularProgressIndicator();

              default:
                if (snapshot.hasData) {
                  return snapshot.data;
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

            // By default, show a loading spinner}
            }
          }
      );
    });
  }
  @override
  void initState() {
    floatingButton1 = new FloatingActionButton(
      onPressed: (){
        toggleFavorite(true);
      },
      tooltip: 'Un-favorite',
      elevation: 10.0,
      child: new Icon(Icons.favorite_border),
    );
    floatingButton0 = new FloatingActionButton(
      onPressed: (){
        toggleFavorite(false);
      },
      tooltip: 'Favorite',
      child: new Icon(Icons.favorite),
    );
    floatingButton = FutureBuilder<FloatingActionButton>(
      future: checkFavorite(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return null;
          case ConnectionState.waiting:
            return new CircularProgressIndicator();

          default:
            if (snapshot.hasData) {
              return snapshot.data;
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

        // By default, show a loading spinner}
        }
      }
      );
  }

  @override
  Widget build(BuildContext context) {
    return
      new Scaffold(
        appBar: new AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text("Cinema Detail"),
        ),
        body: new Center(
          child:
          FutureBuilder<MovieDetail>(
            future: fetchPost(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return
                  new ListView(
                      children: <Widget>[
                        new Text("\n${snapshot.data.title} (${snapshot.data.year})\n",style: new TextStyle(fontSize: 25.0),textAlign: TextAlign.center,),
                        new Container(
                            height: 300.0,
                            child: Image.network(snapshot.data.poster)
                        ),
                        new ListTile(
                          subtitle:new Text("${snapshot.data.tagline}",style: new TextStyle(fontSize: 18.0),textAlign: TextAlign.center,),
                        ),
                        new ListTile(
                          title: new Text("Rating:",textAlign: TextAlign.center,),
                          subtitle: new Text("${snapshot.data.rating}",style: new TextStyle(fontSize: 18.0),textAlign: TextAlign.center,)
                        ),
                        new ListTile(
                            title: new Text("Genre:",textAlign: TextAlign.center,),
                            subtitle:new Text("${snapshot.data.genre}",style: new TextStyle(fontSize: 18.0),textAlign: TextAlign.center,),
                        ),
                        new ListTile(
                          title: new Text("Plot:",textAlign: TextAlign.center,),
                          subtitle:new Text("${snapshot.data.plot}",style: new TextStyle(fontSize: 16.0),textAlign: TextAlign.center,),
                        ),
                        new ListTile(
                          title: new Text("Production:",textAlign: TextAlign.center,),
                          subtitle:new Text("${snapshot.data.prod}",style: new TextStyle(fontSize: 18.0),textAlign: TextAlign.center,),
                        ),
                        new ListTile(
                          title: new Text("Language:",textAlign: TextAlign.center,),
                          subtitle:new Text("${snapshot.data.language}\n\n",style: new TextStyle(fontSize: 18.0),textAlign: TextAlign.center,),
                        ),
                      ]
                  );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return CircularProgressIndicator();
            },
          )
          /*new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("$title\n",style: new TextStyle(fontSize: 25.0),textAlign: TextAlign.center,),
              new Icon(Icons.photo,size: 99.0,),
              new Text("\n$rating",style: new TextStyle(fontSize: 20.0),textAlign: TextAlign.center,),
              new Text("\n$plot", textAlign: TextAlign.center,style: new TextStyle(fontSize: 15.0))
            ],
          )*/
        ),
        floatingActionButton: floatingButton
      );
  }
}